<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FA_Router extends CI_Router {

	function _set_request($segments = array()) {
		if (
			isset($segments[0]) &&
			$segments[0] != 'logged' &&
			$segments[0] != 'api') {
			array_unshift($segments, "frontend");
		}

		// var_dump($segments);
		parent::_set_request($segments);
	}
}

/* End of file FA_Router.php */
/* Location: .//D/Dropbox/xampp/htdocs/2015/09/hoahoc/app/core/FA_Router.php */