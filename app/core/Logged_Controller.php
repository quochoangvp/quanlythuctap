<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logged_Controller extends FA_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->helper('security');

		if (!is_logged()) {
			redirect('login');
		}
	}
}