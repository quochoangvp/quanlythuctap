<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FA_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
		/**
		 * Gọi model thao tác với bảng users
		 */
		$this->load->model('user_model');
		/**
		 * Gọi thư viện làm việc với form
		 */
		$this->load->library('form_validation');

		// Cài đặt thẻ bao thông báo lỗi
		$this->form_validation->set_error_delimiters('<p class="help-block">', '</p>');

		$this->form_validation->set_message('required', '%s không được để trống.');
		$this->form_validation->set_message('alpha', '%s chỉ được dùng kí tự thường.');
		$this->form_validation->set_message('valid_email', '%s phải đúng định dạng.');
		$this->form_validation->set_message('min_length', '%s phải lớn hơn %s kí tự.');
		$this->form_validation->set_message('max_length', '%s phải nhỏ hơn %s kí tự.');
		$this->form_validation->set_message('matches', '%s không trùng với %s.');
		$this->form_validation->set_message('is_unique', '%s này đã tồn tại.');
	}

}